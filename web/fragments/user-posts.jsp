<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h2><span>User Posts:</span></h2>
<div class="user-posts-wrapper">

    <%--@elvariable id="userPosts" type="java.util.List"--%>
    <%--@elvariable id="post" type="com.isoft.socialnetwork.domain.entities.Post"--%>
    <c:forEach var="post" items="${userPosts}">
        <div class="single-post">
            <div class="post-title"><span>Title: </span>${post.title}</div>
            <div class="post-content"><span>Content: </span>${post.content}</div>
            <fmt:parseDate value="${ post.createdOn }" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both"/>
            <span>Created on:</span>
            <small class="text-muted"><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ parsedDateTime }"/></small>
            <button class="btn btn-danger btn-sm" onclick="deletePost(${post.creator.id},${post.id})">Remove</button>
            <button class="btn btn-info btn-sm">Edit</button>
        </div>
    </c:forEach>
</div>
