<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h2 class="user-header">${user.getUsername()}
    <div>
        <button class="btn btn-outline-danger btn-rounded waves-effect" onclick='deleteUser(${user.id})'>DELETE</button>
    </div>
</h2>
<div class="top-side">
    <div class="left-side">
        <div class="user-info"><span>Email:</span> ${user.email}</div>
        <div class="user-info"><span>Age:</span> ${user.age}</div>
        <div class="user-info"><span>Date of join:</span>
            <div>${user.joinDate.dayOfMonth}/${user.joinDate.month}/${user.joinDate.year} ${user.joinDate.dayOfWeek}</div>
            <div>${user.joinDate.hour}:${user.joinDate.minute}</div>
        </div>
    </div>
    <div class="right-side">
        <div class="user-info"><span>User Posts:</span>
            <c:forEach var="post" items="${user.posts}">
                <div class="single-post">
                    <div>${post.title}</div>
                    <div>${post.content}</div>
                    <button onclick="deletePost(${user.id}, ${post.id})" class="btn btn-danger">Remove</button>
                </div>
            </c:forEach>
        </div>
        <nav class="posts-pages">
            <ul class="pagination pagination-sm pagination-circle justify-content-center pg-teal">
                <li class="page-item first"><a class="page-link"><span>First</span></a></li>
                <li class="page-item previous"><a class="page-link"><span>«</span></a></li>
                <li class="page-item next"><a class="page-link"><span>»</span></a></li>
                <li class="page-item last"><a class="page-link"><span>Last</span></a></li>
            </ul>
        </nav>
    </div>
</div>
<div class="bottom-side">
    <section class="more-options">
        <aside class="user-related">
            <div class="select-wrapper">
                <select class="mdb-select md-form dropdown-warning all-users-select">
                </select>
            </div>
            <button class="btn btn-light-green" onclick='addFriend(this, ${user.id})'>ADD FRIEND</button>

            <div class="user-info"><span>User Friends:</span></div>
            <c:forEach var="friend" items="${user.friends}">
                <div>
                <span class="d-flex">
                    <a class="w-25 mw-25 p-3" onclick="loadSingleUser(${friend.id})">${friend.username}</a>

                       <button onclick="unfriend(${user.id}, ${friend.id})" class="btn btn-dark-green btn-sm">Unfriend</button>
                </span>
                </div>
            </c:forEach>
        </aside>
        <aside class="post-related">

            <div class="md-form">
                <input type="text" id="post-title" class="form-control">
                <label for="post-title">Post title</label>
            </div>
            <div class="md-form">
                <textarea id="post-content" class="md-textarea form-control" rows="3"></textarea>
                <label for="post-content">Post content</label>
            </div>
            <button onclick="addPost(${user.id})" class="btn btn-success btn-lg">Add post</button>
        </aside>
    </section>
</div>