<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Social network</title>
    <link href="font/roboto/Roboto-Light.woff2" rel="stylesheet">
    <link href="font/roboto/Roboto-Light.woff" rel="stylesheet">
    <link href="font/roboto/Roboto-Light.ttf" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="libs/bootstrap.css"/>
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="libs/mdb.css"/>

    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<h1>Hello, JavaEE</h1>
<div class="top">
    <div class="create-user-form">
        <form id="new-user-form" action="${pageContext.request.contextPath}/user/" method="POST">
            <div class="md-form">
                <input type="text" id="username" name="username" class="form-control">
                <label for="username">Username</label>
            </div>
            <div class="md-form">
                <input type="text" id="email" name="email" class="form-control">
                <label for="email">Email</label>
            </div>
            <div class="md-form">
                <input type="number" id="age" name="age" class="form-control">
                <label for="age">Age</label>
            </div>
            <div class="form-group">
                <input class="btn btn-primary btn-lg" id="new-user-button" type="submit" value="Create user">
            </div>
        </form>
    </div>
    <div id="users-container">
        <ul id="users">
        </ul>

        <nav class="pages">
            <ul class="pagination justify-content-center pagination-circle pg-amber">
                <li class="page-item first"><a class="page-link"><span>First</span></a></li>
                <li class="page-item previous"><a class="page-link"><span>«</span></a></li>
                <li class="page-item next"><a class="page-link"><span>»</span></a></li>
                <li class="page-item last"><a class="page-link"><span>Last</span></a></li>
            </ul>
        </nav>
    </div>
</div>
<div class="view">
    <div id="single-user-view-container">

    </div>
</div>


<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="libs/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript"
        src="libs/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="libs/mdb.js"></script>

<script src="postman.js"></script>
<%--<script src="pagination-script.js"></script>--%>
<script src="Paginator.js"></script>
</body>
</html>
