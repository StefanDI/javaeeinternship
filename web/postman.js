/**
 *
 * Code here is for testing purposes ONLY
 * Do not try to make sense of it, if you value your sanity
 *
 * Update: Code is a bit more readable now,but still chaotic
 *
 */
// $(document).ajaxError((errResp) => {
//     console.log("AJAX ERROR" + errResp)
// });
$(document).ready(() => {

    $.get({
        url: 'user?count',
        success: function (count) {
            let UsersPaginator =
                new Paginator("user/getAll", 6, ".pages", count, renderUsers);
            UsersPaginator.fillPage(1);
        }
    });

    //Attach new user listener
    document.getElementById("new-user-button")
        .addEventListener("click", (e) => {
            makeNewUser(e);
        });

});

function renderUsers(resp) {

    $("#users").empty();
    resp.forEach((user) => {
        renderUser(user);
    });
}

function deleteUser(userId) {
    let url = "user/" + userId;
    $.ajax({
        url: url,
        type: 'DELETE',
        success: function () {
            $("#single-user-view-container").empty();
            fillPage(currentPage);
        }
    });
}

function addFriend(btn, userId) {

    // let friendId = $($(btn).next()).val();
    let select = $(".all-users-select")[1];
    let friendId = select.options[select.selectedIndex].value;
    let url = `user/addFriend?user-id=${userId}&friend-id=${friendId}`;
    $.post({
        url: url,
        success: function () {
            //Refresh the current user view, to update friends
            $("#" + userId)[0].click();
        }
    })
}

/**
 * Fetch
 * @param user
 */
function renderFullUser(user) {
    $.get({
        url: "user/getOne?id=" + user.id,
        success: function (resp) {
            $.get({
                url: "post/getByUser?user-id=" + user.id + "&count",
                success: (resp) => {
                    let PostsPaginator =
                        new Paginator(`/post/getByUser?user-id=${user.id}`,
                            2,
                            ".posts-pages",
                            resp,
                            renderPosts);
                    PostsPaginator.fillPage(1);
                }
            });

            $("#single-user-view-container")[0].innerHTML = resp;

            let select = $(".all-users-select");

            $(select).material_select();


            $.get({
                url: "user/getAll",
                success: (allUsers) => {
                    //TODO: revert to normal select or optimize somehow
                    allUsers = allUsers.slice(1, 10);
                    //TODO: make backend function to fetch users not friend of
                    $(select).append('<option value="" disabled selected>Select friend to add</option>');
                    $.each(allUsers, function () {
                        if (this.id !== user.id)
                            $(select).append($("<option />").val(this.id).text(this.username));
                    });

                }
            });


        }
    });
}

/**
 * renders a single user container,with the username in it, in the list of users
 * and attaches event listener on click render user with more options and info (FullUser :) )
 * @param user the user
 */
function renderUser(user) {
    let singleUser = $(`<h2 id="${user.id}" class='single-user'>`)
        .on("click",
            () => {
                renderFullUser(user);
            });

    let usrnm = $("<div>").text(`${user.username}`);

    $(singleUser).append(usrnm);

    $("#users").append(singleUser);
}

/**
 * Prepare and send post request to /user {create new user}
 * @param e the click event
 */
function makeNewUser(e) {
    e.preventDefault();
    let form = document.getElementById("new-user-form");
    let data = {};
    for (let i = 0; i < form.length; ++i) {
        let input = form[i];
        if (input.name) {
            data[input.name] = input.value;
        }
    }

    let xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    // send the collected data as JSON
    xhr.send(JSON.stringify(data));

    xhr.onloadend = function () {
        $("#single-user-view-container").empty();
        fillPage(currentPage);
    };
}

/**
 * Simulates a click at the desired user
 * @param userId
 */
function loadSingleUser(userId) {
    $("#" + userId)[0].click();
}

function unfriend(userId, friendId) {
    $.post({
        url: `user/removeFriend?user-id=${userId}&friend-id=${friendId}`,
        success: () => {
            loadSingleUser(userId);
        }
    })
}

function addPost(userId) {
    let postTitle = $("#post-title").val();
    let postContent = $("#post-content").val();

    $.post({
        url: "post?userId=" + userId,
        data: JSON.stringify({
            title: postTitle,
            content: postContent
        }),
        contentType: "application/json",
        success: () => {
            loadSingleUser(userId);
        }
    })

}

/**
 * Deletes the post by id and loads the user which id is supplied(used for refreshing the info)
 * @param userId the userId
 * @param postId the postId
 */
function deletePost(userId, postId) {
    $.ajax({
        url: `post?postId=${postId}`,
        type: 'DELETE',
        success: () => {
            loadSingleUser(userId);
        }
    })
}

function renderPosts(posts) {
    let wrapper = $(".right-side > .user-info");
    $(wrapper)[0].innerHTML = posts;
}