class Paginator {

    _url;
    _itemsPerPage;
    _parent;
    _itemsCount;
    _renderFunction;

    currentPage = 1;

    constructor(url, itemsPerPage, paginationBarParent, itemsCount, renderFunction) {
        this._url = url;
        this._itemsPerPage = itemsPerPage;
        this._parent = paginationBarParent;
        this._itemsCount = itemsCount;
        this._renderFunction = renderFunction;

        this.populatePaginationBar();
        $(paginationBarParent + " .page-link").click(this.pageClicked.bind(this));
    }


    fillPage(pageNumber) {
        let offset = pageNumber <= 1 ? 0 : (pageNumber - 1) * this._itemsPerPage;
        let url = this._url.indexOf("?") > 0 ?
            `${this._url}&limit=${this._itemsPerPage}&offset=${offset}`
            ://If its a query add parameter("&"), else start with query("?")
            `${this._url}?limit=${this._itemsPerPage}&offset=${offset}`;


        $.ajax({
            url: url,
            method: "GET",
            success: (resp) => {
                this._renderFunction(resp)
            }, error: (error) => {
                //Some crappy bug with jquery, server returns 200 OK, but it goes into error
                if (error.status === 200)
                    this._renderFunction(error.responseText);
            }
        });

    };


    populatePaginationBar() {
        let allPagesCount = Math.ceil(this._itemsCount / this._itemsPerPage);

        //Show only 5 pages at once
        let pageStartNumber = (this.currentPage - 2) <= 0 ? 1 : this.currentPage - 2;
        let pageEndNumber = (this.currentPage + 2) > allPagesCount ? allPagesCount : this.currentPage + 2;
        //First page
        if (this.currentPage === 1) {
            $(this._parent + " .previous").addClass("disabled");
            $(this._parent + " .first").addClass("disabled");
            $(this._parent + " .next").removeClass("disabled");
            $(this._parent + " .last").removeClass("disabled");
        }
        //Last page
        else if (this.currentPage === allPagesCount) {
            $(this._parent + " .next").addClass("disabled");
            $(this._parent + " .last").addClass("disabled");
            $(this._parent + " .previous").removeClass("disabled");
            $(this._parent + " .first").removeClass("disabled");
        }
        //No words to show
        else if (this._itemsCount === 0) {
            $(this._parent + " .previous").addClass("disabled");
            $(this._parent + " .first").addClass("disabled");
            $(this._parent + " .next").addClass("disabled");
            $(this._parent + " .last").addClass("disabled");
        } else {
            $(this._parent + " .previous").removeClass("disabled");
            $(this._parent + " .first").removeClass("disabled");
            $(this._parent + " .next").removeClass("disabled");
            $(this._parent + " .last").removeClass("disabled");
        }


        //Rerender pages every time
        let a = $(this._parent + " > .pagination > *:not(.first, .previous, .last, .next)");

        a.remove();
        for (let i = pageStartNumber; i <= pageEndNumber; i++) {
            let pageLi = $("<li>").addClass("page-item");
            let pageLink = $("<a>").addClass("page-link");
            pageLi.append(pageLink);
            pageLink.text(i);

            if (i === this.currentPage) {
                pageLi.addClass("active");
            }

            pageLink.click(this.pageClicked.bind(this));

            $(pageLi).insertBefore(this._parent + " .next");
        }

    };


    pageClicked(event) {

        let link = $(event.target);
        let liItemParent = link.parent();

        //Sometimes click is registered on the <span>, sometimes on the <a>
        //if the click is registered on the <span> get the parent's parent
        if (liItemParent.attr("class").toString().includes("page-link"))
            liItemParent = liItemParent.parent();


        if (liItemParent.attr("class").toString().includes("previous")) {
            this.currentPage = this.currentPage - 1;
        } else if (liItemParent.attr("class").toString().includes("next")) {
            this.currentPage = this.currentPage + 1;
        } else if (liItemParent.attr("class").toString().includes("first")) {
            this.currentPage = 1;
        } else if (liItemParent.attr("class").toString().includes("last")) {
            this.currentPage = Math.ceil(this._itemsCount / this._itemsPerPage);
        } else {
            this.currentPage = parseInt(link.text());
        }

        this.populatePaginationBar();
        this.fillPage(this.currentPage);
    };
}