package com.isoft.socialnetwork.utils;

/**
 * @author Stefan Ivanov
 */
public class DBUsersColumns
{
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String AGE = "age";
    public static final String JOIN_DATE = "date_of_join";

}
