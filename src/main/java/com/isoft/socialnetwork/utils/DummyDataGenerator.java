package com.isoft.socialnetwork.utils;

import com.isoft.socialnetwork.domain.entities.User;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author Stefan Ivanov
 */
public class DummyDataGenerator
{
    private DummyDataGenerator()
    {

    }

    public static List<User> generateDummyUsers(int amount)
    {
        String[] names = {"Ivan", "Petyr", "Ekaterina", "Gosho", "Stefan", "Maria", "Misho", "Georgi", "Kiro"};
        String[] emails = {"nekavMail@abv.bg", "drugMail@mail.bg", "thebeser@mail.bg", "nuynear@gmail.com", "a582hs@gmail.com"};

        List<User> users = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < amount; i++)
        {
            String randomName = names[r.nextInt(names.length)] + i;
            String randomEmail = emails[r.nextInt(emails.length)] + i;
            int randomAge = r.nextInt(100 - 18) + 18;
            long randomMilliseconds = r.nextLong();
            LocalDateTime randomDate = new Date(randomMilliseconds)
                    .toInstant()
                    .atZone(
                            ZoneId.systemDefault())
                    .toLocalDateTime();

            User randomUser = new User(randomName, randomEmail, randomAge, randomDate);
            users.add(randomUser);
        }
        return users;
    }
}
