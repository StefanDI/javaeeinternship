package com.isoft.socialnetwork.utils;

/**
 * @author Stefan Ivanov
 */
public class DBPostsColumns
{
    public static final String POST_ID = "post_id";
    public static final String POST_CONTENT = "content";
    public static final String POST_TITLE = "title";
    public static final String POST_CREATED_ON = "created_on";
    public static final String POST_CREATOR_ID = "creator_id";

}
