package com.isoft.socialnetwork.utils.mappers;

import com.isoft.socialnetwork.domain.entities.Post;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.utils.DBPostsColumns;
import com.isoft.socialnetwork.utils.DBUsersColumns;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Maps 'raw' entity data (represented as key - value pairs where key- column name & value - column value)
 * to the actual entity
 *
 * @author Stefan Ivanov
 */
public class DBRawToEntitiesMapper
{

    private static final String POST_ID = "post_id";
    private static final String POST_CONTENT_COLUMN = "content";
    private static final String POST_TITLE_COLUMN = "title";
    private static final String POST_CREATED_ON_COLUMN = "created_on";


    private DBRawToEntitiesMapper()
    {
    }

    public static User mapToUser(Map<String, Object> rawUser)
    {
        User user = new User();

        user.setId((Integer) rawUser.get(DBUsersColumns.USER_ID));
        user.setUsername((String) rawUser.get(DBUsersColumns.USERNAME));
        user.setEmail((String) rawUser.get(DBUsersColumns.EMAIL));
        user.setAge((Integer) rawUser.getOrDefault(DBUsersColumns.AGE, 0));
        user.setJoinDate(((Timestamp) rawUser.get(DBUsersColumns.JOIN_DATE)).toLocalDateTime());

        return user;
    }

    public static Post mapToPost(Map<String, Object> rawPost, User creator)
    {
        Post p = new Post();

        p.setId((Integer) rawPost.get(POST_ID));
        p.setTitle((String) rawPost.get(POST_TITLE_COLUMN));
        p.setContent((String) rawPost.get(POST_CONTENT_COLUMN));
        p.setCreatedOn(((Timestamp) rawPost.get(POST_CREATED_ON_COLUMN)).toLocalDateTime());
        p.setCreator(creator);

        return p;
    }

    public static List<User> mapToUserList(List<Map<String, Object>> rawUsers)
    {
        List<User> mappedUsers = new ArrayList<>();
        for (Map<String, Object> user : rawUsers)
        {
            mappedUsers.add(mapToUser(user));
        }
        return mappedUsers;
    }
}
