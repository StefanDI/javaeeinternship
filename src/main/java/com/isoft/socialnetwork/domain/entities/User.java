package com.isoft.socialnetwork.domain.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Stefan Ivanov
 */
public class User
{

    private int id;
    private String username;
    private String email;
    private int age;
    private LocalDateTime joinDate;
    private List<User> friends;
    private List<Post> posts;

    public User()
    {
    }

    public User(String username,
                String email,
                int age,
                LocalDateTime joinDate)
    {
        this.username = username;
        this.email = email;
        this.age = age;
        this.joinDate = joinDate;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public LocalDateTime getJoinDate()
    {
        return joinDate;
    }

    public void setJoinDate(LocalDateTime joinDate)
    {
        this.joinDate = joinDate;
    }

    public List<User> getFriends()
    {
        if (friends == null)
            friends = new ArrayList<>();
        return friends;
    }

    public void setFriends(List<User> friends)
    {
        this.friends = friends;
    }

    public void addFriend(User userToBefriend)
    {
        this.getFriends().add(userToBefriend);
    }

    public List<Post> getPosts()
    {
        if(this.posts == null)
            this.posts = new ArrayList<>();
        return posts;
    }

    public void setPosts(List<Post> posts)
    {
        this.posts = posts;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                username.equals(user.username) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, username, email);
    }
}
