package com.isoft.socialnetwork.domain.entities;

import java.time.LocalDateTime;

/**
 * @author Stefan Ivanov
 */
public class Post
{
    private int id;
    private String title;
    private String content;
    private LocalDateTime createdOn;
    private User creator;

    public Post()
    {
    }

    public Post(String title, String content, LocalDateTime createdOn, User creator)
    {
        this.title = title;
        this.content = content;
        this.createdOn = createdOn;
        this.creator = creator;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public LocalDateTime getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn)
    {
        this.createdOn = createdOn;
    }

    public User getCreator()
    {
        return creator;
    }

    public void setCreator(User creator)
    {
        this.creator = creator;
    }
}
