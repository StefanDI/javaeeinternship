package com.isoft.socialnetwork.filters;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.utils.RequestIOUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Validates if requests are valid
 * and returns BadRequest with error message if not
 *
 * @author Stefan Ivanov
 */
public class UsersRequestFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //Get the last part of the url, after the last "/"
        String endPoint = request.getRequestURI()
                .substring(request.getRequestURI().lastIndexOf("/") + 1);
        switch (request.getMethod())
        {
            case "POST":
            {
                if (endPoint.equalsIgnoreCase("addFriend") ||
                        endPoint.equalsIgnoreCase("removeFriend"))
                {
                    if (request.getParameter("user-id") == null
                            || request.getParameter("friend-id") == null)
                    {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Must provide user-id and friend-id");
                        return;
                    }
                } else
                {   //Create user
                    try
                    {
                        User u = RequestIOUtils.parseJSONtoObject(request, User.class);
                        if (u.getUsername() == null || u.getUsername().isEmpty())
                        {
                            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Username cannot be null or empty");
                            return;
                        }
                        if (u.getEmail() == null || u.getEmail().isEmpty())
                        {
                            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Email cannot be null or empty");
                            return;
                        }
                    } catch (JsonMappingException | JsonParseException jsonException)
                    {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid JSON");
                        return;
                    } catch (IOException ioe)
                    {
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "There was a problem reading the request");
                        return;
                    }
                }

            }
            break;
            case "GET":
            {
                if (endPoint.equalsIgnoreCase("getOne"))
                {
                    if (request.getParameter("id") == null)
                    {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "There is no user id");
                        return;
                    }
                }

                if (!endPoint.equalsIgnoreCase("getOne")
                        && !endPoint.equalsIgnoreCase("getAll")
                        && request.getParameter("count") == null
                        && !endPoint.equalsIgnoreCase("user"))
                {
                    response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "Unsupported get operation");
                    return;
                }
            }
            break;
            case "DELETE":
            {
                //TODO: Validate somehow, maybe check if there is user with such id or something
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {

    }
}
