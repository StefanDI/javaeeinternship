package com.isoft.socialnetwork.filters;

import com.isoft.socialnetwork.servlets.PostServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Validates urls, going to {@link PostServlet}
 * TODO: Add all validations
 *
 * @author Stefan Ivanov
 */
public class PostsRequestFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        switch (request.getMethod())
        {
            case "GET":
            {
                if (request.getParameterMap().isEmpty() || request.getParameter("user-id") == null)
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "You must provide user-id parameter");
                filterChain.doFilter(request, response);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {

    }
}
