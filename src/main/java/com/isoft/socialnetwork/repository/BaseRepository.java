package com.isoft.socialnetwork.repository;

import java.util.List;
import java.util.Map;

public interface BaseRepository<T>
{
    /**
     * Inserts an entity into the database
     *
     * @param entity the entity
     */
    void insert(T entity);

    /**
     * Finds a single entity by given column name and column value
     *
     * @param columnName  the column name criteria
     * @param columnValue the value of the column to look for
     * @return the entity
     */
    T findBy(String columnName, Object columnValue);

    /**
     * @return all entities in the table
     */
    List<T> findAll();

    /**
     * Paginates the requested data
     *
     * @param pageNumber   the current page number
     * @param itemsPerPage the items per page
     * @return List<Entity> with size = itemsPerPage
     */
    List<T> findAllPaginated(int pageNumber, int itemsPerPage);

    /**
     * Finds more than 1 entity by criteria
     *
     * @param columnName  the column name
     * @param columnValue the value of the column
     * @return empty list, if no results were found
     */

    List<T> findAllBy(String columnName, Object columnValue);

    /**
     * Deletes the entity
     *
     * @param entity the entity to be deleted
     */
    void delete(T entity);

    /**
     * Deletes entity by given criteria
     *
     * @param columnName  the column name
     * @param columnValue the value of the column
     */
    void deleteBy(String columnName, Object columnValue);

    /**
     * Updates all columns of the given entity
     * (including ManyToMany, OneToMany and ManyToOne relations also)
     *
     * @param entity the entity to update
     */
    void update(T entity);

    /**
     * Updates only some columns
     *
     * @param fieldsToUpdate key-> column name ; value -> new column value
     */
    void update(int entityId, Map<String, Object> fieldsToUpdate);

    /**
     * @return the count of entities in the database
     */
    int count();

}
