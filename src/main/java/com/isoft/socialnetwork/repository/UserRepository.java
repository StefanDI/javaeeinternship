package com.isoft.socialnetwork.repository;

import com.isoft.socialnetwork.domain.entities.User;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public interface UserRepository extends BaseRepository<User>
{
    /**
     * Finds all friends by given userId
     *
     * @param id the id of the user which friends we look for
     * @return empty list if the user has no friends (  ForeverAlone :(  )
     */

    List<User> findAllFriends(int id);

    /**
     * Checks if the user has the other user as a friend
     *
     * @param userId   the user
     * @param friendId the id of the other user to check if he is friend
     * @return if the user with userId has friend with friendId
     */
    boolean hasFriend(int userId, int friendId);
}
