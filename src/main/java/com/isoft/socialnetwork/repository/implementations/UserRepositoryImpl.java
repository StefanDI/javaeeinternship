package com.isoft.socialnetwork.repository.implementations;

import com.isoft.socialnetwork.db.JDBCConnector;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.repository.UserRepository;
import com.isoft.socialnetwork.utils.DBUsersColumns;
import com.isoft.socialnetwork.utils.mappers.DBRawToEntitiesMapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * This represents the DAO Api, creating queries to the database
 * through JDBC {@link JDBCConnector class}
 * and mapping the result to {@link User} domain model
 * <p>
 * UPDATE: it wont return null anymore, rather an empty {@link User} object
 * The service layer can later check if the user is valid
 *
 * @author Stefan Ivanov
 */
public class UserRepositoryImpl implements UserRepository
{
    private static final String USER_TABLE_NAME = "users";

    private final JDBCConnector db;

    public UserRepositoryImpl(JDBCConnector db)
    {
        this.db = db;
    }

    @Override
    public void insert(User u)
    {
        String username = u.getUsername();
        String email = u.getEmail();
        int age = u.getAge();
        LocalDateTime dateOfJoin = u.getJoinDate();

        String query = "INSERT INTO users(username,email,age,date_of_join)" +
                "VALUES(?,?,?,?)";

        this.db.executeQuery(query, username, email, age, dateOfJoin);
    }

    @Override
    public User findBy(String columnName, Object columnValue)
    {
        String query = String.format("SELECT * FROM %s WHERE %s = '%s'",
                USER_TABLE_NAME, columnName, columnValue);

        return DBRawToEntitiesMapper.mapToUser(this.db.executeQueryWithSingleResult(query));
    }

    @Override
    public List<User> findAll()
    {
        String query = "SELECT * FROM " + USER_TABLE_NAME;

        return DBRawToEntitiesMapper.
                mapToUserList(
                        db.executeQueryWithMultipleResult(query));
    }

    @Override
    public List<User> findAllBy(String columnName, Object columnValue)
    {
        String query;
        query = String.format("SELECT * FROM %s WHERE %s = '%s'",
                USER_TABLE_NAME, columnName, columnName);

        return DBRawToEntitiesMapper.
                mapToUserList(
                        db.executeQueryWithMultipleResult(query));

    }

    @Override
    public void delete(User entity)
    {
        String unformattedQuery = "DELETE FROM %s WHERE %s = '%s'";
        String query;
        //If the user has invalid id, or no id, delete by username
        if (entity.getId() <= 0)
        {
            query = String.format(unformattedQuery,
                    USER_TABLE_NAME,
                    DBUsersColumns.USERNAME,
                    entity.getUsername());
        } else
        {
            query =
                    String.format(unformattedQuery,
                            USER_TABLE_NAME,
                            DBUsersColumns.USER_ID,
                            entity.getId());
        }

        this.db.executeQuery(query);
    }

    @Override
    public void deleteBy(String columnName, Object value)
    {
        String query =
                String.format("DELETE FROM %s WHERE %s = '%s'",
                        USER_TABLE_NAME, columnName, value);

        this.db.executeQuery(query);
    }

    @Override
    public void update(User u)
    {
        //If the user has invalid userId simply return
        if (u.getId() <= 0)
            return;
        int userId = u.getId();
        String username = u.getUsername();
        String email = u.getEmail();
        int age = u.getAge();

        String query =
                String.format("UPDATE %s " +
                                "SET %s = ?," +
                                " %s = ?, " +
                                "%s = ? WHERE %s = '%s'",
                        USER_TABLE_NAME,
                        DBUsersColumns.USERNAME,
                        DBUsersColumns.EMAIL,
                        DBUsersColumns.AGE,
                        DBUsersColumns.USER_ID,
                        userId);


        this.db.executeQuery(query, username, email, age);


        List<User> userFriends = findAllFriends(u.getId());
        //Adds all friends which are in the User's friend's List<User>
        addNewFriendsToDB(u, userFriends);
        //Remove all friends who are in the database, but not in the user's
        //List<User> friends
        removeOldFriendsFromDB(u, userFriends);

    }

    @SuppressWarnings("StringConcatenationInLoop")
    @Override
    public void update(int userId, Map<String, Object> fieldsToUpdate)
    {
        if (fieldsToUpdate.isEmpty())
            return;

        Iterator<Map.Entry<String, Object>> iter =
                fieldsToUpdate.entrySet().iterator();

        String query = "UPDATE " + USER_TABLE_NAME + "SET ";
        while (true)
        {
            Map.Entry<String, Object> entry = iter.next();
            query += entry.getKey() + "=" + entry.getValue();

            //If iter has next, add comma , else break the loop
            if (iter.hasNext())
            {
                query += ",";
            } else
            {
                break;
            }
        }

        query += " WHERE " + DBUsersColumns.USER_ID + " = " + userId;

        this.db.executeQuery(query);
    }

    @Override
    public List<User> findAllFriends(int id)
    {
        List<User> userFriends = new ArrayList<>();

        List<Map<String, Object>> userFriendsMap =
                this.db.executeQueryWithMultipleResult("SELECT * FROM user_friends WHERE user_id = " + id);
        for (Map<String, Object> friendRaw : userFriendsMap)
        {
            User friend = this.findBy(DBUsersColumns.USER_ID, friendRaw.get("friend_id"));
            userFriends.add(friend);
        }

        return userFriends;
    }

    @Override
    public boolean hasFriend(int userId, int friendId)
    {
        String query =
                String.format("SELECT * FROM user_friends WHERE user_id = %s AND friend_id = %s",
                        userId,
                        friendId);

        return this.db.executeQueryWithSingleResult(query).isEmpty();
    }

    private void removeOldFriendsFromDB(User u, List<User> userFriends)
    {
        int userId = u.getId();

        for (User userFriend : userFriends)
        {
            if (!u.getFriends().contains(userFriend))
            {
                int friendId = userFriend.getId();
                this.db.executeQuery(
                        "DELETE FROM user_friends WHERE user_id = ? AND friend_id = ?",
                        userId,
                        friendId);

                //Because of the bidirectional ManyToMany mapping
                //Remove the user from the userFriend's friend list too
                this.db.executeQuery(
                        "DELETE FROM user_friends WHERE user_id = ? AND friend_id = ?",
                        friendId,
                        userId);
            }
        }
    }

    private void addNewFriendsToDB(User u, List<User> userFriends)
    {
        int userId = u.getId();
        for (User friend : u.getFriends())
        {
            //Ignore the entry if its already added to the DB
            if (userFriends.contains(friend))
                continue;

            int friendId =
                    this.findBy(DBUsersColumns.USERNAME, friend.getUsername())
                            .getId();

            this.db.executeQuery("INSERT INTO user_friends(user_id,friend_id)" +
                            "VALUES(?,?)",
                    userId,
                    friendId);

            //This is for the bidirectional ManyToMany mapping
            this.db.executeQuery("INSERT INTO user_friends(user_id,friend_id)" +
                            "VALUES(?,?)",
                    friendId,
                    userId);
        }
    }

    @Override
    public List<User> findAllPaginated(int pageNumber, int itemsPerPage)
    {
        String query =
                String.format("SELECT * FROM %s " +
                                "ORDER BY %s " +
                                "LIMIT %s OFFSET %s",
                        USER_TABLE_NAME,
                        DBUsersColumns.JOIN_DATE,
                        itemsPerPage, pageNumber);

        return DBRawToEntitiesMapper.mapToUserList(
                db.executeQueryWithMultipleResult(query)
        );
    }

    @Override
    public int count()
    {
        return db.getCount(USER_TABLE_NAME, "");
    }
}