package com.isoft.socialnetwork.repository.implementations;

import com.isoft.socialnetwork.db.JDBCConnector;
import com.isoft.socialnetwork.domain.entities.Post;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.repository.PostRepository;
import com.isoft.socialnetwork.utils.DBPostsColumns;
import com.isoft.socialnetwork.utils.mappers.DBRawToEntitiesMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Stefan Ivanov
 */
public class PostRepositoryImpl implements PostRepository
{
    private static final String POST_TABLE_NAME = "posts";
    private final JDBCConnector db;

    public PostRepositoryImpl(JDBCConnector db)
    {
        this.db = db;
    }

    public void insert(Post post)
    {
        String postTitle = post.getTitle();
        String postContent = post.getContent();
        LocalDateTime createdOn = LocalDateTime.now();
        User creator = post.getCreator();

        db.executeQuery("INSERT INTO POSTS(title,content,created_on,creator_id)" +
                "VALUES(?,?,?,?)", postTitle, postContent, createdOn, creator.getId());
    }

    @Override
    public Post findBy(String columnName, Object columnValue)
    {
        String query =
                String.format("SELECT * FROM %s WHERE %s = '%s'",
                        POST_TABLE_NAME,
                        columnName,
                        columnValue);
        Map<String, Object> rawPost = db.executeQueryWithSingleResult(query);

        User postCreator = getPostCreator(rawPost);

        return DBRawToEntitiesMapper.mapToPost(rawPost, postCreator);
    }

    @Override
    public List<Post> findAll()
    {
        String query = "SELECT * FROM " + POST_TABLE_NAME;
        List<Map<String, Object>> listOfRawPosts = db.executeQueryWithMultipleResult(query);
        List<Post> mappedPosts = new ArrayList<>();
        for (Map<String, Object> singleRawPost : listOfRawPosts)
        {
            User creator = getPostCreator(singleRawPost);
            mappedPosts.add(DBRawToEntitiesMapper.mapToPost(singleRawPost, creator));
        }
        return mappedPosts;
    }

    @Override
    public List<Post> findAllBy(String columnName, Object columnValue)
    {
        String query =
                String.format("SELECT * FROM %s WHERE %s = '%s'",
                        POST_TABLE_NAME,
                        columnName,
                        columnValue);

        List<Post> mappedPosts = new ArrayList<>();
        return extractMappedList(query, mappedPosts);
    }

    @Override
    public void delete(Post entity)
    {
        String query =
                String.format("DELETE FROM %s WHERE %s = %s",
                        POST_TABLE_NAME,
                        DBPostsColumns.POST_ID,
                        entity.getId());

        this.db.executeQuery(query);
    }

    @Override
    public void deleteBy(String columnName, Object columnValue)
    {

        String query =
                String.format("DELETE FROM %s WHERE %s = %s",
                        POST_TABLE_NAME,
                        columnName,
                        columnValue);

        this.db.executeQuery(query);
    }

    @Override
    public void update(Post entity)
    {
        String newPostTitle = entity.getTitle();
        String newPostContent = entity.getContent();

        String query =
                String.format("UPDATE %s " +
                                "SET %s  = %s," +
                                "%s = %s",
                        POST_TABLE_NAME,
                        DBPostsColumns.POST_TITLE, newPostTitle,
                        DBPostsColumns.POST_CONTENT, newPostContent);

        this.db.executeQuery(query);
    }

    @Override
    public void update(int entityId, Map<String, Object> fieldsToUpdate)
    {
        //This is actually a duplicate code i have transform a bit
        //TODO: Find a way to pull logic somewhere for reusability
        String query = "UPDATE " + POST_TABLE_NAME + "SET ";
        String[] columns = fieldsToUpdate.keySet().toArray(new String[0]);
        for (int i = 0; i < columns.length; i++)
        {
            query += columns[i] + " = " + fieldsToUpdate.get(columns[i]);
            if (i < columns.length - 1)
                query += ",";
        }

        this.db.executeQuery(query);
    }

    @Override
    public int count()
    {
        return db.getCount(POST_TABLE_NAME, "");
    }

    @Override
    public int count(int userId)
    {
        String whereClause = "WHERE creator_id = " + userId;
        return db.getCount(POST_TABLE_NAME, whereClause);
    }

    @Override
    public List<Post> findPostsByUser(int userId, int pageSize, int pageNumber)
    {
        List<Post> userPosts = new ArrayList<>();

        String query =
                String.format(
                        "SELECT * FROM %s WHERE creator_id = %s " +
                                "ORDER BY %s " +
                                "LIMIT %s OFFSET %s",
                        POST_TABLE_NAME, userId,
                        DBPostsColumns.POST_CREATED_ON,
                        pageSize <= 0 ? "ALL" : pageSize, pageNumber);

        return extractMappedList(query, userPosts);
    }

    @Override
    public List<Post> findAllPaginated(int pageNumber, int itemsPerPage)
    {
        throw new NotImplementedException();
    }

    private List<Post> extractMappedList(String query, List<Post> mappedPosts)
    {
        List<Map<String, Object>> listOfRawPosts =
                this.db.executeQueryWithMultipleResult(query);
        for (Map<String, Object> singleRawPost : listOfRawPosts)
        {
            User creator = getPostCreator(singleRawPost);
            mappedPosts.add(DBRawToEntitiesMapper.mapToPost(singleRawPost, creator));
        }

        return mappedPosts;
    }

    private User getPostCreator(Map<String, Object> rawPost)
    {
        return DBRawToEntitiesMapper.mapToUser(
                this.db.executeQueryWithSingleResult(
                        "SELECT * FROM users WHERE user_id = '" + rawPost.get("creator_id") + "'"));
    }
}