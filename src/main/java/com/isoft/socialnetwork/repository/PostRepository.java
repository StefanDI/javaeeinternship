package com.isoft.socialnetwork.repository;

import com.isoft.socialnetwork.domain.entities.Post;

import java.util.List;

public interface PostRepository extends BaseRepository<Post>
{
    /**
     * Returns posts by user paged,
     * if pageSize and pageNumber is 0
     * returns all posts by the user
     *
     * @param userId     the id of the user
     * @param pageSize   the page size
     * @param pageNumber the page number
     * @return posts by user by page
     */
    List<Post> findPostsByUser(int userId, int pageSize, int pageNumber);

    /**
     * Return the amount of posts the user has
     *
     * @param userId the user id
     * @return the count of posts
     */
    int count(int userId);
}
