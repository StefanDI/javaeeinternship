package com.isoft.socialnetwork.db;


import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * The holder of the connection class
 *
 * @author Stefan Ivanov
 */
public class JDBCConnector
{
    private static final Logger LOGGER = Logger.getLogger(JDBCConnector.class);
    private Connection conn;

    public JDBCConnector(String jdbcDriver,
                         String dbUrl,
                         String user,
                         String password) throws SQLException, ClassNotFoundException
    {
        //No idea how this works, but it does
        Class.forName(jdbcDriver);

        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", password);
        props.setProperty("ssl", "true");
        conn = DriverManager.getConnection(dbUrl, props);

    }

    //TODO: move this logic outside of this class
    public void initTables()
    {
        try (Statement stmt = conn.createStatement())
        {
            //TODO: replace hard coded column names with those from utils.DBUsersColumns
            String createUsersQuery =
                    "CREATE TABLE IF NOT EXISTS USERS(" +
                            "user_id SERIAL PRIMARY KEY," +
                            "username VARCHAR(30) NOT NULL UNIQUE ," +
                            "email VARCHAR(255) NOT NULL UNIQUE, " +
                            "age INT," +
                            "date_of_join TIMESTAMP NOT NULL ) ";

            String createPostQuery = "CREATE TABLE IF NOT EXISTS POSTS(" +
                    "post_id SERIAL PRIMARY KEY ," +
                    "content VARCHAR(255) NOT NULL ," +
                    "title VARCHAR(16) NOT NULL, " +
                    "created_on TIMESTAMP NOT NULL ," +
                    "creator_id INT NOT NULL, " +
                    "FOREIGN KEY (creator_id) REFERENCES USERS(user_id) ON UPDATE CASCADE ON DELETE CASCADE ) ";

            String createUserFriendsQuery = "CREATE TABLE IF NOT EXISTS USER_FRIENDS(" +
                    "user_id SERIAL REFERENCES USERS(user_id) ON UPDATE CASCADE ON DELETE CASCADE," +
                    "friend_id SERIAL REFERENCES USERS(user_id) ON UPDATE CASCADE ON DELETE CASCADE ," +
                    "CONSTRAINT user_friends_pk PRIMARY KEY (user_id, friend_id))";


            stmt.executeUpdate(createUsersQuery);
            LOGGER.info("CREATED USERS SUCCESSFULLY");
            stmt.executeUpdate(createPostQuery);
            LOGGER.info("CREATED POSTS SUCCESSFULLY");
            stmt.executeUpdate(createUserFriendsQuery);
            LOGGER.info("CREATED USER MANY TO MANY SUCCESSFULLY");
        } catch (SQLException e)
        {
            LOGGER.fatal("FAILED TO CREATE TABLES ", e);
        }

    }

    /**
     * example INSERT INTO users(username, password) VALUES(?,?)
     * wildcard params are the params for the query ("?")
     *
     * @param query          the query
     * @param wildcardParams the params for the wildcard
     */
    public void executeQuery(String query, Object... wildcardParams)
    {
        //Intellij suggested naming 'ignored'
        try (Statement ignored = conn.createStatement();
             PreparedStatement pstmt = conn.prepareStatement(query))
        {
            for (int i = 0; i < wildcardParams.length; i++)
            {
                /*
                 For some reason LocalDateTime
                 is not accepted for {@link PreparedStatement#setObject(int, Object)}
                 */
                if (wildcardParams[i] instanceof LocalDateTime)
                    pstmt.setTimestamp(i + 1, Timestamp.valueOf((LocalDateTime) wildcardParams[i]));
                else
                    pstmt.setObject(i + 1, wildcardParams[i]);
            }

            pstmt.executeUpdate();
            LOGGER.info("Successfully executed query: " + pstmt);
        } catch (SQLException e)
        {
            LOGGER.error("Failed to execute query: " + query +
                    "With values " +
                    Arrays.toString(wildcardParams), e);
        }
    }

    /**
     * Executes a SELECT query with multiple results
     * every entity is represented as Map<String, Object>
     * where String is the column name, Object is the column value
     * if there are no entities from the result return empty List
     *
     * @param query the query
     * @return list of entities represented as described above
     */
    public List<Map<String, Object>> executeQueryWithMultipleResult(String query)
    {
        List<Map<String, Object>> objectsList = new ArrayList<>();
        //Intellij suggested naming 'ignored'
        try (Statement stmt = conn.createStatement();
             PreparedStatement ignored = conn.prepareStatement(query))
        {
            ResultSet r = stmt.executeQuery(query);
            objectsList = parseResultSetToMap(r);
            LOGGER.info("Successfully fetched query: " + query);
        } catch (SQLException e)
        {
            LOGGER.error("Failed to execute query: " + query, e);
        }
        return objectsList;
    }

    /**
     * Get count of all rows of given table
     *
     * @param tableName the table name
     * @param whereClause where clause for the count query (optional)
     * @return the number of rows
     */
    public int getCount(String tableName, String whereClause)
    {
        String query = "SELECT COUNT(*) FROM " + tableName +
                (whereClause == null ? "" : " " + whereClause);
        try (Statement stmt = conn.createStatement())
        {
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e)
        {
            LOGGER.error("Error executing query " + query, e);
        }
        return -1;
    }

    /**
     * execute a SELECT query where the result is only 1 entity
     *
     * @param query the query
     * @return a key - value pair -> column name - column value, empty map if no entity is found
     */
    public Map<String, Object> executeQueryWithSingleResult(String query)
    {
        Map<String, Object> resultColumnsMap = new HashMap<>();

        //Intellij suggested naming 'ignored'
        try (Statement stmt = conn.createStatement();
             PreparedStatement ignored = conn.prepareStatement(query))
        {
            ResultSet r = stmt.executeQuery(query);
            //Since we are sure it will be only 1 entity, and parseResultSetToMap returns List<Map>
            //we get the first element of the List
            //if the result returned no values, it will throw IndexOutOfBoundException
            //and will return empty Map
            resultColumnsMap = parseResultSetToMap(r).get(0);
            LOGGER.info("Successfully fetched query: " + query);
            return resultColumnsMap;
        } catch (SQLException e)
        {
            LOGGER.error("Failed to execute query " + query, e);
        } catch (IndexOutOfBoundsException iob)
        {
            //Thrown when #parseResultSetToMap returns empty list ( ResultSet was empty)
            LOGGER.info("No results found for query: " + query);
        }

        return resultColumnsMap;
    }

    private List<Map<String, Object>> parseResultSetToMap(ResultSet r) throws SQLException
    {
        ResultSetMetaData metadata = r.getMetaData();
        int columnCount = metadata.getColumnCount();

        List<Map<String, Object>> entitiesList = new ArrayList<>();

        int index = 0;
        //For each entity (r.next) iterate all its columns
        //and put them in a map where key is column name
        //and value is column value
        while (r.next())
        {
            entitiesList.add(new HashMap<>());

            for (int i = 1; i <= columnCount; i++)
            {
                String col = metadata.getColumnName(i);
                Object value = r.getObject(col);
                entitiesList.get(index).put(col, value);
            }
            index++;
        }
        return entitiesList;
    }
}