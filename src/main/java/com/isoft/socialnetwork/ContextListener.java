package com.isoft.socialnetwork;

import com.isoft.socialnetwork.db.JDBCConnector;
import com.isoft.socialnetwork.repository.PostRepository;
import com.isoft.socialnetwork.repository.implementations.PostRepositoryImpl;
import com.isoft.socialnetwork.repository.implementations.UserRepositoryImpl;
import com.isoft.socialnetwork.services.implementations.PostServiceImpl;
import com.isoft.socialnetwork.services.implementations.UserServiceImpl;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Stefan Ivanov
 */
public class ContextListener implements ServletContextListener
{
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    private String dbDriver;
    private String dbUrl;
    private String dbName;
    private String dbUser;
    private String dbPass;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        try
        {
            Properties props = new Properties();
            URL appPropertiesUrl =
                    getClass().getClassLoader().getResource("application.properties");

            URL log4jPropertiesUrl =
                    getClass().getClassLoader().getResource("log4j.properties");

            if (log4jPropertiesUrl != null)
                props.load(new FileInputStream(new File(log4jPropertiesUrl.getPath())));

            PropertyConfigurator.configure(props);

            if (appPropertiesUrl == null)
            {
                LOGGER.info("application.properties not found, reverting to default options\n");
                loadDefault();
            } else
            {
                File f = new File(appPropertiesUrl.getPath());
                props.load(new FileInputStream(f));
                this.dbDriver = props.getProperty("jdbc.driver").trim();
                this.dbUrl = props.getProperty("db.url").trim();
                this.dbName = props.getProperty("db.name").trim();
                this.dbUser = props.getProperty("user").trim();
                this.dbPass = props.getProperty("pass").trim();
                LOGGER.info("Loaded application.properties successfully \n");
            }
        } catch (IOException e)
        {
            LOGGER.info("Error loading application.properties, loading default ", e);
            loadDefault();
        }

        try
        {
            //Make connection
            JDBCConnector connector = new JDBCConnector(dbDriver, dbUrl + dbName, dbUser, dbPass);
            LOGGER.info("Built JDBC connection successfully ");
            //Init tables if they dont exist
            connector.initTables();
            LOGGER.info("INITIALIZED ALL TABLES SUCCESSFULLY");

            PostRepository postRepository = new PostRepositoryImpl(connector);
            //BUILD SERVICE SINGLETONS
            new UserServiceImpl.Builder()
                    .getBuilder()
                    .setUserRepository(new UserRepositoryImpl(connector))
                    .setPostRepository(postRepository)
                    .build();
            new PostServiceImpl.Builder()
                    .getBuilder()
                    .setPostRepository(postRepository)
                    .setUserService(UserServiceImpl.getService())
                    .build();
        } catch (SQLException e)
        {
            LOGGER.error("Error connection to the database ", e);
        } catch (ClassNotFoundException e)
        {
            LOGGER.error("Wrong JDBC driver, or none found ", e);
        }
    }

    private void loadDefault()
    {
        this.dbDriver = "org.postgresql.Driver";
        this.dbUrl = "jdbc:postgresql://localhost:5432/";
        this.dbName = "social_network";
        this.dbUser = "stefan";
        this.dbPass = "87hspq2";

        //TODO: Maybe its not a good idea to log the user and pass of the DB
        LOGGER.info(
                String.format(
                        "Loaded default properties:%nJDBC driver: %s%n" +
                                "Database url: %s%n" +
                                "Database name: %s%n" +
                                "User: %s%n" +
                                "Password: %s%n ",
                        dbDriver, dbUrl, dbName, dbUser, dbPass));
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        LOGGER.info("SERVER STOPPED");
    }
}
