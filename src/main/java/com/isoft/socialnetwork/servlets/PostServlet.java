package com.isoft.socialnetwork.servlets;

import com.isoft.socialnetwork.domain.entities.Post;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.services.PostService;
import com.isoft.socialnetwork.services.UserService;
import com.isoft.socialnetwork.services.implementations.PostServiceImpl;
import com.isoft.socialnetwork.services.implementations.UserServiceImpl;
import com.isoft.socialnetwork.utils.RequestIOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * GET:
 * /post/getByUser?user={id}    //Also works with offset & limit for paginated results
 *                              //Also works with &count parameter, for amount of posts of given user
 * POST:
 * /post?userId= {creatorId}    Builds a {@link Post} entity from JSON data in the request
 *                              and {creatorId} as a post creator
 * Delete
 * /post?postId={postId}        Deletes the post with {postId}
 * @author Stefan Ivanov
 */
public class PostServlet extends HttpServlet
{
    private PostService postService;

    @Override
    public void init() throws ServletException
    {
        this.postService = PostServiceImpl.getService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String userId = req.getParameter("user-id");
        if(req.getParameter("count") != null)
            RequestIOUtils.writeJSONResp(resp, postService.getPostsCount(userId));
        else
        {
            int pageSize = 0;
            int pageNumber = 0;
            if(req.getParameter("offset") != null)
                pageNumber = Integer.parseInt(req.getParameter("offset"));
            if(req.getParameter("limit") != null)
                pageSize = Integer.parseInt(req.getParameter("limit"));

            List<Post> postsByUserPaged = postService.getPostsByUserPaged(userId, pageSize, pageNumber);
            req.setAttribute("userPosts", postsByUserPaged);
            req.getRequestDispatcher("/fragments/user-posts.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        int userId = Integer.parseInt(req.getParameter("userId"));
        User creator = new User();
        creator.setId(userId);
        Post p = RequestIOUtils.parseJSONtoObject(req, Post.class);
        p.setCreator(creator);
        postService.insertPost(p);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String postId = req.getParameter("postId");
        this.postService.removePost(postId);
    }


}
