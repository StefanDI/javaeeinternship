package com.isoft.socialnetwork.servlets;

import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.services.UserService;
import com.isoft.socialnetwork.services.implementations.UserServiceImpl;
import com.isoft.socialnetwork.utils.RequestIOUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Listens on /user/*
 * supports the following:
 * GET :
 * /user/getOne?id=someId  -> return user by id
 * /user/getAll -> return all users (accepts offset & limit for pagination)
 * /user?count -> count of all users
 * POST :
 * /user -> insert the user given in request body as JSON
 * /user/addFriend?userId=id&friendId=id2 -> add a friend with given friendId to the user with id
 * its actually bidirectional so they are both added as friends
 * DELETE :
 * /user/{userIdToDelete}  - delete the user with the given ID
 *
 * @author Stefan Ivanov
 */
public class UserServlet extends HttpServlet
{
    private UserService userService;

    @Override
    public void init() throws ServletException
    {
        this.userService = UserServiceImpl.getService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String endPoint = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);

        if (endPoint.equalsIgnoreCase("getAll"))
        {
            if (!req.getParameterMap().isEmpty())
            {
                int page = Integer.parseInt(req.getParameter("offset"));
                int pageSize = Integer.parseInt(req.getParameter("limit"));
                List<User> users = userService.getPaged(page, pageSize);
                RequestIOUtils.writeJSONResp(resp, users);
                return;
            }
            List<User> allUsers = userService.getAll();
            RequestIOUtils.writeJSONResp(resp, allUsers);
        } else if (endPoint.equalsIgnoreCase("getOne"))
        {
            String userId = req.getParameter("id");
            User u = userService.findUserById(userId);
            //Return the single-user.jsp, as template to the front end
            req.setAttribute("user", u);
            req.getRequestDispatcher("/fragments/single-user.jsp").forward(req, resp);
        } else if(req.getParameter("count") != null)
        {
            RequestIOUtils.writeJSONResp(resp, userService.count());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String endPoint = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);

        if (endPoint.equalsIgnoreCase("addFriend"))
        {
            String userId = req.getParameter("user-id");
            String userIdToBefriend = req.getParameter("friend-id");

            this.userService.makeFriends(userId, userIdToBefriend);

        } else if (endPoint.equalsIgnoreCase("removeFriend"))
        {
            String userId = req.getParameter("user-id");
            String friendId = req.getParameter("friend-id");
            this.userService.unFriend(userId, friendId);
        } else
        {
            User u = RequestIOUtils.parseJSONtoObject(req, User.class);
            UserServiceImpl.getService().insertUser(u);
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
    {
        String userIdToDelete =
                req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);

        UserServiceImpl.getService().removeUserById(userIdToDelete);
    }


}
