package com.isoft.socialnetwork.services;

import com.isoft.socialnetwork.domain.entities.Post;
import com.isoft.socialnetwork.domain.entities.User;

import java.util.List;

/**
 * Basic operations with Posts
 * methods are self explanatory
 * @author Stefan Ivanov
 */
public interface PostService
{

    void insertPost(Post post);

    void removePost(String postId);

    Post findById(String postId);

    List<Post> getPostsByUserPaged(String userId, int pageSize, int pageNumber);

    List<Post> getAll();

    int getPostsCount(String userId);
}
