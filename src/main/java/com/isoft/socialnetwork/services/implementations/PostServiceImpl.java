package com.isoft.socialnetwork.services.implementations;

import com.isoft.socialnetwork.domain.entities.Post;
import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.repository.PostRepository;
import com.isoft.socialnetwork.services.PostService;
import com.isoft.socialnetwork.services.UserService;
import com.isoft.socialnetwork.utils.DBPostsColumns;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class PostServiceImpl implements PostService
{
    private static PostService _this;

    private final PostRepository postRepository;
    private final UserService userService;

    public static PostService getService()
    {
        return _this;
    }

    private PostServiceImpl(PostRepository postRepository,
                            UserService userService)
    {
        this.postRepository = postRepository;
        this.userService = userService;
    }

    @Override
    public void insertPost(Post post)
    {
        //IF for some reason id of the user is not specified
        //Search for the user by username (its unique)
        if (post.getCreator().getId() <= 0)
        {
            User creator = userService.findUserByUsername(post.getCreator().getUsername());
            post.setCreator(creator);
        }

        this.postRepository.insert(post);
    }

    @Override
    public void removePost(String postId)
    {
        Post p = this.findById(postId);
        postRepository.delete(p);
    }

    @Override
    public Post findById(String postId)
    {
        return this.postRepository.findBy(DBPostsColumns.POST_ID, postId);
    }

    @Override
    public List<Post> getPostsByUserPaged(String userId, int pageSize, int pageNumber)
    {

        return postRepository.findPostsByUser(Integer.parseInt(userId), pageSize, pageNumber);
    }

    @Override
    public int getPostsCount(String userId)
    {
        return postRepository.count(Integer.parseInt(userId));
    }

    @Override
    public List<Post> getAll()
    {
        return this.postRepository.findAll();
    }

    public static class Builder
    {

        private PostRepository postRepository;
        private UserService userService;

        public Builder getBuilder()
        {
            return this;
        }

        public Builder setPostRepository(PostRepository postRepository)
        {
            this.postRepository = postRepository;
            return this;
        }

        public Builder setUserService(UserService userService)
        {
            this.userService = userService;
            return this;
        }

        public void build()
        {
            _this = new PostServiceImpl(postRepository, userService);
        }
    }
}
