package com.isoft.socialnetwork.services.implementations;

import com.isoft.socialnetwork.domain.entities.User;
import com.isoft.socialnetwork.repository.PostRepository;
import com.isoft.socialnetwork.repository.UserRepository;
import com.isoft.socialnetwork.services.UserService;
import com.isoft.socialnetwork.utils.DBUsersColumns;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class UserServiceImpl implements UserService
{
    private static UserService instance;

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    public static UserService getService()
    {
        return instance;
    }

    private UserServiceImpl(UserRepository userRepository, PostRepository postRepository)
    {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @Override
    public void insertUser(User u)
    {
        u.setJoinDate(LocalDateTime.now());
        this.userRepository.insert(u);
    }


    @Override
    public void removeUserById(String userId)
    {
        this.userRepository.deleteBy(DBUsersColumns.USER_ID, userId);
    }

    @Override
    public void updateUser(User u)
    {
        this.userRepository.update(u);
    }

    @Override
    public void makeFriends(String userId, String friendId)
    {
        User user = this.findUserById(userId);
        User userToBefriend = this.findUserById(friendId);
        user.addFriend(userToBefriend);

        this.updateUser(user);
    }

    @Override
    public void unFriend(String userId, String friendId)
    {
        User u1 = this.findUserById(userId);
        User u2 = this.findUserById(friendId);

        u1.getFriends().remove(u2);
        u2.getFriends().remove(u1);

        this.updateUser(u1);
        this.updateUser(u2);
    }

    @Override
    public User findUserById(String id)
    {
        User user =
                this.userRepository.findBy(DBUsersColumns.USER_ID, id);


        user.setFriends(this.getUserFriends(user));
        return user;
    }

    @Override
    public User findUserByUsername(String username)
    {
        User user =
                this.userRepository.findBy(DBUsersColumns.USERNAME, username);


        user.setFriends(getUserFriends(user));
        return user;
    }

    @Override
    public List<User> getUserFriends(User u)
    {
        //If there is no id set
        if (u.getId() <= 0)
            u = this.findUserByUsername(u.getUsername());


        return new ArrayList<>(this.userRepository.findAllFriends(u.getId()));
    }

    @Override
    public List<User> getAll()
    {
        List<User> allUsers = new ArrayList<>();

        //Add every user's friend
        for (User user : this.userRepository.findAll())
        {
            user.setFriends(this.getUserFriends(user));
            allUsers.add(user);
        }

        return allUsers;
    }

    @Override
    public int count()
    {
        return this.userRepository.count();
    }

    @Override
    public List<User> getPaged(int pageNumber, int pageSize)
    {
        List<User> friendlessUsers = userRepository.findAllPaginated(pageNumber, pageSize);

        friendlessUsers.forEach((soonNotToBeFriendless) ->
                soonNotToBeFriendless.setFriends(getUserFriends(soonNotToBeFriendless)));

        return friendlessUsers;
    }

    public static class Builder
    {
        private UserRepository userRepository;
        private PostRepository postRepository;

        public Builder getBuilder()
        {
            return this;
        }

        public Builder setUserRepository(UserRepository userRepository)
        {
            this.userRepository = userRepository;
            return this;
        }

        public Builder setPostRepository(PostRepository postRepository)
        {
            this.postRepository = postRepository;
            return this;
        }

        //Since its singleton it doesnt return the UserService
        public void build()
        {
            if (postRepository == null || userRepository == null)
                throw new IllegalStateException("Cannot instantiate User Service without PostRepository and UserRepository");

            instance = new UserServiceImpl(userRepository, postRepository);
        }
    }
}
