package com.isoft.socialnetwork.services;

import com.isoft.socialnetwork.domain.entities.User;

import java.util.List;

/**
 * Basic operations with users,
 * methods are self explanatory so no javadoc on each method
 * (dont know if i should)
 * @author Stefan Ivanov
 */
public interface UserService
{
    /**
     * Also adds date of creation - now
     * @param u the user
     */
    void insertUser(User u);

    void removeUserById(String userId);

    void updateUser(User u);

    void makeFriends(String userId, String friendId);

    void unFriend(String userId, String friendId);

    User findUserById(String id);

    User findUserByUsername(String username);

    List<User> getUserFriends(User u);

    List<User> getAll();

    int count();

    List<User> getPaged(int pageNumber, int pageSize);
}
